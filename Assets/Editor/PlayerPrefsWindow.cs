﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEditor;
using UnityEngine;

public class PlayerPrefsWindow : EditorWindow
{
    [MenuItem("Window/PlayerPrefs")]
    private static void Init()
    {
        // Get existing open window or if none, make a new one:
        PlayerPrefsWindow window = GetWindow<PlayerPrefsWindow>();
        window.Show();
    }

    private static string key = "";
    private static int value = -1;

    private void OnGUI()
    {
        key = EditorGUILayout.TextField(key);
        value = EditorGUILayout.IntField("Reišmė", value);

        if (GUILayout.Button("Atnaujinti"))
        {
            value = PlayerPrefs.GetInt(key, 0);
            Focus();
            Repaint();
        }

        if (GUILayout.Button("Įrašyti"))
        {
            PlayerPrefs.SetInt(key, value);
            Repaint();
        }

        if (GUILayout.Button("Reset"))
        {
            PlayerPrefs.DeleteAll();
            PlayerPrefs.Save();

            value = 0;

            Repaint();
        }

        Repaint();
    }
}