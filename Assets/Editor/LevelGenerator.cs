﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class LevelGenerator : EditorWindow
{
    [MenuItem("Window/My Window")]
    private static void Init()
    {
        // Get existing open window or if none, make a new one:
        LevelGenerator window = (LevelGenerator)EditorWindow.GetWindow(typeof(LevelGenerator));
        window.Show();
    }

    private int from = 0;
    private int to = 50;
    private int tries = 1000;
    private int mirrors = 4;
    private float probability = 1;
    private int increasePerCount = 20;
    private int decreasePerCount = 100;

    private void OnGUI()
    {
        if (GUILayout.Button("Deconstruct"))
        {
            LevelConstructor.Deconstruct();
        }
        if (GUILayout.Button("Reconstruct"))
        {
            LevelConstructor.Reconstruct();
        }
        if (GUILayout.Button("GenerateLevel"))
        {
            LevelConstructor.Generate(mirrors, tries, from, to, probability);
        }
        if (GUILayout.Button("GenerateLevelWithDiversity"))
        {
            LevelConstructor.Generate(mirrors, tries, from, to, probability, increasePerCount, decreasePerCount);
        }
        mirrors = EditorGUILayout.IntField("Mirrors: ", mirrors);
        from = EditorGUILayout.IntField("From: ", from);
        to = EditorGUILayout.IntField("To: ", to);
        tries = EditorGUILayout.IntField("Tries: ", tries);
        probability = EditorGUILayout.FloatField("Mirror probability: ", probability);
        increasePerCount = EditorGUILayout.IntField("Increase per count: ", increasePerCount);
        decreasePerCount = EditorGUILayout.IntField("Decrease per count: ", decreasePerCount);
        Repaint();
    }
}