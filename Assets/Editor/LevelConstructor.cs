﻿using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;
using System.Text;
using System.Linq;
using System;
using UnityEditor.SceneManagement;
using UnityEngine.SceneManagement;

public class LevelConstructor : Editor
{
    [MenuItem("Custom/Deconstruct levels &d")]
    public static void Deconstruct()
    {
        var objects = Resources.LoadAll<GameObject>("Levels").ToArray();

        foreach (var o in objects)
        {
            List<string> prefabs = new List<string>();

            for (int i = 0; i < o.transform.childCount; i++)
            {
                var child = o.transform.GetChild(i);

                if (child.name.StartsWith("Finish"))
                {
                    prefabs.Add(string.Format("F;{0};{1}", child.position.x, child.position.y));
                }
                else if (child.name.StartsWith("Reflective"))
                {
                    prefabs.Add(string.Format("R;{0};{1};{2};{3};{4}", child.position.x, child.position.y, child.rotation.eulerAngles.z, child.localScale.x, child.localScale.y));
                }
                else if (child.name.StartsWith("Transmitive"))
                {
                    prefabs.Add(string.Format("T;{0};{1};{2};{3};{4}", child.position.x, child.position.y, child.rotation.eulerAngles.z, child.localScale.x, child.localScale.y));
                }
            }

            var b = new StringBuilder();

            foreach (var item in prefabs)
            {
                b.AppendLine(item);
            }

            using (var sw = new StreamWriter("Assets/Resources/Levels/" + o.name + ".txt"))
            {
                sw.WriteLine(b);
            }
            Debug.Log("Exported to " + o.name + ".txt\n" + b);
        }
        AssetDatabase.Refresh();
    }

    public static void Generate(int mirrors, int tries, int from, int to, float probab, int increase = 99999, int decrease = 999999)
    {
        var beginMirros = mirrors;
        var beginProbab = probab;

        var gameObjects = Resources.LoadAll<GameObject>("Prefabs").ToArray();

        var level = gameObjects.FirstOrDefault(x => x.name == "Level");
        var finish = gameObjects.FirstOrDefault(x => x.name == "Finish");
        var reflective = gameObjects.FirstOrDefault(x => x.name == "Reflective");
        var transmitive = gameObjects.FirstOrDefault(x => x.name == "Transmitive");
        var player = SceneManager.GetActiveScene().GetRootGameObjects().FirstOrDefault(x => x.name == "Torch");

        var startPosition = player.transform.position;
        int wrong = 0;

        for (int cc = from; cc < to && wrong < tries; cc++)
        {
            mirrors = beginMirros + Mathf.RoundToInt(Mathf.Floor(1.0f / increase * (cc - from)));
            probab = Mathf.Clamp01(beginProbab - Mathf.RoundToInt(Mathf.Floor(.1f / decrease * (cc - from))));

            var angle = UnityEngine.Random.Range(-80, 80);
            // Debug.DrawLine(startPosition, startPosition + Quaternion.Euler(0, 0, angle) * Vector3.up, Color.green, 10);
            GameObject cur = (GameObject)PrefabUtility.InstantiatePrefab(level);
            Vector3[] path;
            bool blog = false;

            for (int i = 0; i < mirrors; i++)
            {
                path = LightBeam.BeamShot(startPosition, Quaternion.Euler(0, 0, angle) * Vector3.up, 200, 1.1f);
                Debug.Log(path.Select(x => x.ToString()).ToList().Aggregate((r, t) => r + " " + t));

                var distance = UnityEngine.Random.Range(1.5f, 3.5f);
                Vector3 temp = path[path.Length - 1] - path[path.Length - 2];
                temp.Normalize();
                temp *= distance;
                Vector3 mirrorPos = path[path.Length - 2] + temp;
                Vector3 mirrorDir = Quaternion.Euler(0, 0, UnityEngine.Random.Range(-80, 80)) * temp;
                Vector3 dist = mirrorDir.normalized * UnityEngine.Random.Range(-0.3f, 0.3f);
                mirrorPos += dist;
                GameObject refl;

                if (UnityEngine.Random.Range(0f, 1f) <= probab)
                    refl = (GameObject)PrefabUtility.InstantiatePrefab(reflective);
                else
                    refl = (GameObject)PrefabUtility.InstantiatePrefab(transmitive);

                refl.transform.SetParent(cur.transform);
                refl.transform.position = mirrorPos;
                mirrorDir.Normalize();
                refl.transform.eulerAngles = new Vector3(0, 0, Mathf.Rad2Deg * Mathf.Atan2(mirrorDir.y, mirrorDir.x) - 90);

                Vector3 temp2 = refl.transform.position + refl.transform.up * refl.GetComponent<BoxCollider2D>().size.y / 2;

                if (temp2.y < -3.7 || temp2.y > 4.8 || temp2.x < -2.8 || temp2.x > 2.8)
                {
                    blog = true;
                    break;
                }

                temp2 = refl.transform.position - refl.transform.up * refl.GetComponent<BoxCollider2D>().size.y / 2;

                if (temp2.y < -3.7 || temp2.y > 4.8 || temp2.x < -2.8 || temp2.x > 2.8)
                {
                    blog = true;
                    break;
                }
            }

            path = LightBeam.BeamShot(startPosition, Quaternion.Euler(0, 0, angle) * Vector3.up, 200, 1.1f);

            {
                var distance = UnityEngine.Random.Range(2, 4);
                Vector3 temp = path[path.Length - 1] - path[path.Length - 2];
                temp.Normalize();
                temp *= distance;
                Vector3 finishPos = path[path.Length - 2] + temp;
                var refl = (GameObject)PrefabUtility.InstantiatePrefab(finish);
                refl.transform.SetParent(cur.transform);
                refl.transform.SetAsFirstSibling();
                refl.transform.position = finishPos;

                Vector3 temp2 = refl.transform.position;

                if (temp2.y < -1.5 || temp2.y > 4.4 || temp2.x < -2.2 || temp2.x > 2.2)
                {
                    blog = true;
                }
            }

            var path1 = LightBeam.BeamShot(startPosition, Quaternion.Euler(0, 0, angle + 0.5f) * Vector3.up, 200, 1.1f);
            var path2 = LightBeam.BeamShot(startPosition, Quaternion.Euler(0, 0, angle - 0.5f) * Vector3.up, 200, 1.1f);

            if (blog || path.Length <= mirrors || path1.Length <= mirrors || path2.Length <= mirrors)
            {
                //Debug.Log(wrong);
                wrong++;
                cc--;
                DestroyImmediate(cur);
                continue;
            }

            Debug.Log(cc + " " + angle);
            PrefabUtility.CreatePrefab(string.Format("Assets/Resources/LevelsGen/level{0:0000}.prefab", cc), cur);
            DestroyImmediate(cur);
        }
    }

    [MenuItem("Custom/Reconstruct levels &f")]
    public static void Reconstruct()
    {
        var gameObjects = Resources.LoadAll<GameObject>("Prefabs").ToArray();

        var level = gameObjects.FirstOrDefault(x => x.name == "Level");
        var finish = gameObjects.FirstOrDefault(x => x.name == "Finish");
        var reflective = gameObjects.FirstOrDefault(x => x.name == "Reflective");
        var transmitive = gameObjects.FirstOrDefault(x => x.name == "Transmitive");

        var objects = Resources.LoadAll<TextAsset>("Levels").ToArray();

        foreach (var levelText in objects)
        {
            var lines = levelText.text.Split('\r', '\n');
            var primeGO = (GameObject)PrefabUtility.InstantiatePrefab(level);

            foreach (var line in lines)
            {
                var data = line.Split(';');

                switch (data[0])
                {
                    case "F":
                        var finishGO = (GameObject)PrefabUtility.InstantiatePrefab(finish);
                        finishGO.transform.SetParent(primeGO.transform);
                        finishGO.transform.position = new Vector3(float.Parse(data[1]), float.Parse(data[2]), 0);
                        break;

                    case "R":
                        var reflectiveGO = (GameObject)PrefabUtility.InstantiatePrefab(reflective);
                        reflectiveGO.transform.SetParent(primeGO.transform);
                        reflectiveGO.transform.position = new Vector3(float.Parse(data[1]), float.Parse(data[2]), 0);
                        reflectiveGO.transform.eulerAngles = new Vector3(0, 0, float.Parse(data[3]));
                        reflectiveGO.transform.localScale = new Vector3(float.Parse(data[4]), float.Parse(data[5]), 1);
                        break;

                    case "T":
                        var transmitiveGO = (GameObject)PrefabUtility.InstantiatePrefab(transmitive);
                        transmitiveGO.transform.SetParent(primeGO.transform);
                        transmitiveGO.transform.position = new Vector3(float.Parse(data[1]), float.Parse(data[2]), 0);
                        transmitiveGO.transform.eulerAngles = new Vector3(0, 0, float.Parse(data[3]));
                        transmitiveGO.transform.localScale = new Vector3(float.Parse(data[4]), float.Parse(data[5]), 1);
                        break;
                }
            }

            PrefabUtility.CreatePrefab("Assets/Resources/Levels/" + levelText.name + ".prefab", primeGO);
            Debug.Log("Created level file: " + levelText.name + ".prefab");
            DestroyImmediate(primeGO);
        }
        AssetDatabase.Refresh();
    }

    [MenuItem("Custom/Destroy Levels &g")]
    public static void Destroy()
    {
        var objects = Resources.LoadAll<GameObject>("Levels").ToArray();

        foreach (var o in objects)
        {
            var path = "Assets/Resources/Levels/" + o.name + ".prefab";

            FileUtil.DeleteFileOrDirectory(path);

            Debug.Log("Destroyed at path: " + path);
        }
        AssetDatabase.Refresh();
    }
}