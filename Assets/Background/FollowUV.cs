﻿using UnityEngine;
using System.Collections;

public class FollowUV : MonoBehaviour {

	public float parralax = 2f;
    Material mat;

    void Start()
    {
        mat = GetComponent<MeshRenderer>().material;
    }

    void Update () {
		Vector2 offset = mat.mainTextureOffset;

        offset.y = Time.time / parralax;

		mat.mainTextureOffset = offset;
	}

}
