﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Audio;                        
public class MainMenu : MonoBehaviour {
    public void ChangeScene(string scene)
    {
        SceneManager.LoadScene(scene);
    }
    public void Quit() 
    {
        Application.Quit();
    }
    
}
