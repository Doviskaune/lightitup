﻿using System;
using System.Collections.Generic;
using UnityEngine;

public static class LightBeam
{
    public static LayerMask endMask = LayerMask.GetMask("End");
    public static LayerMask transmissionMask = LayerMask.GetMask("Transmission");
    public static LayerMask collisionMask = LayerMask.GetMask("Reflection", "End", "Transmission");
    public static float length = 3f;
    public static int endLayer = Mathf.RoundToInt(Mathf.Log(endMask & -endMask, 2));
    public static int transmissionLayer = Mathf.RoundToInt(Mathf.Log(transmissionMask & -transmissionMask, 2));

    public static bool finished;

    public static Vector3[] Beam(Vector3 position, Vector3 direction, int reflectionCount, float c)
    {
        var result = new List<Vector3> { position };
        var currentLength = 0f;

        Collider2D temp = null;

        for (int i = 0; i < reflectionCount; i++)
        {
            direction.Normalize();

            if (temp)
                temp.enabled = false;

            var newLength = Mathf.Min(25, length - currentLength);
            var hit = Physics2D.Raycast(position, direction, newLength, collisionMask);

            if (temp)
                temp.enabled = true;

            if (!hit.collider)
            {
                if (newLength > 0.08f)
                    result.Add(position + direction * newLength);
                break;
            }

            if (hit.transform.gameObject.layer == transmissionLayer)
            {
                var sin = Mathf.Sin(Mathf.Deg2Rad * Vector3.Angle(direction, hit.normal));
                var value = sin * c;
                if (value > 1)
                {
                    result.Add(hit.point);
                    position = hit.point;
                    direction = Vector3.Reflect(direction, hit.normal).normalized;
                }
                else
                {
                    var a = hit.transform.up * ((BoxCollider2D)hit.collider).size.y;
                    var b = direction;
                    var d = (Vector3)hit.point - hit.transform.position;

                    var y = (d.x * a.y - a.x * d.y) / (b.y * a.x - a.y * b.x);
                    float x;
                    if (Math.Abs(a.x) > 0.0000001f)
                        x = (d.x + y * b.x) / a.x;
                    else
                        x = (d.y + y * b.y) / a.y;

                    if (x <= 1 && x >= -1)
                    {
                        var point = hit.transform.position + x * a;
                        result.Add(point);
                        position = point;

                        var angle = Mathf.Asin(value);
                        direction = Vector3.RotateTowards(-hit.normal,
                                Vector3.RotateTowards(hit.normal, direction, Mathf.PI / 2, 0).normalized,
                                angle,
                                0)
                            .normalized;
                    }
                    else
                    {
                        result.Add(hit.point);
                        position = hit.point;
                    }
                }
            }
            else
            {
                result.Add(hit.point);
                position = hit.point;
                direction = Vector3.Reflect(direction, hit.normal).normalized;
            }

            var tempV = (result[result.Count - 1] - result[result.Count - 2]);
            var additionalDistance = tempV.magnitude;
            if (currentLength + additionalDistance > length)
            {
                result[result.Count - 1] = result[result.Count - 2] + tempV.normalized * (length - currentLength);
                return result.ToArray();
            }

            currentLength += additionalDistance;

            temp = hit.collider;
            if (hit.transform.gameObject.layer == endLayer)
                return result.ToArray();
        }

        return result.ToArray();
    }

    public static Vector3[] BeamShot(Vector3 position, Vector3 direction, int reflectionCount, float c)
    {
        finished = false;
        var result = new List<Vector3> { position };

        Collider2D temp = null;

        for (int i = 0; i < reflectionCount; i++)
        {
            direction.Normalize();

            if (temp)
                temp.enabled = false;

            var hit = Physics2D.Raycast(position, direction, 25, collisionMask);

            if (temp)
                temp.enabled = true;

            if (!hit.collider)
            {
                result.Add(position + direction * 25);
                break;
            }

            if (hit.transform.gameObject.layer == transmissionLayer)
            {
                var sin = Mathf.Sin(Mathf.Deg2Rad * Vector3.Angle(direction, hit.normal));
                var value = sin * c;
                if (value > 1)
                {
                    result.Add(hit.point);
                    position = hit.point;
                    direction = Vector3.Reflect(direction, hit.normal).normalized;
                }
                else
                {
                    var a = hit.transform.up * ((BoxCollider2D)hit.collider).size.y / 2;
                    var b = direction;
                    var d = (Vector3)hit.point - hit.transform.position;

                    var y = (d.x * a.y - a.x * d.y) / (b.y * a.x - a.y * b.x);
                    float x;
                    if (Math.Abs(a.x) > 0.0000001f)
                        x = (d.x + y * b.x) / a.x;
                    else
                        x = (d.y + y * b.y) / a.y;

                    if (x <= 1 && x >= -1)
                    {
                        var point = hit.transform.position + x * a;
                        result.Add(point);
                        position = point;

                        var angle = Mathf.Asin(value);
                        direction = Vector3.RotateTowards(-hit.normal,
                                Vector3.RotateTowards(hit.normal, direction, Mathf.PI / 2, 0).normalized,
                                angle,
                                0)
                            .normalized;
                    }
                    else
                    {
                        result.Add(hit.point);
                        position = hit.point;
                    }
                }
            }
            else
            {
                result.Add(hit.point);
                position = hit.point;
                direction = Vector3.Reflect(direction, hit.normal).normalized;
            }

            temp = hit.collider;
            if (hit.transform.gameObject.layer == endLayer)
            {
                finished = true;
                return result.ToArray();
            }
        }

        return result.ToArray();
    }
}