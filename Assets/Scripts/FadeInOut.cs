﻿using UnityEngine;
using System.Collections;

public class FadeInOut : MonoBehaviour
{
    public SpriteRenderer sprite;

    public Color spriteColor = Color.white;
    public float fadeInTime = 0.8f;
    public float fadeOutTime = 0.8f;
    public float delayToFadeOut = 0f;
    public float delayToFadeIn = 0f;
    public bool animate = true;

    public void StartAnimation()
    {
        StartCoroutine("FadeCycle");
    }

    IEnumerator FadeCycle()
    {
        float fade = 0f;
        float startTime;
        while (animate)
        {
            startTime = Time.time;
            while (fade < 1f)
            {
                fade = Mathf.Lerp(0f, 1f, (Time.time - startTime) / fadeInTime);
                spriteColor.a = fade;
                sprite.color = spriteColor;
                yield return null;
            }
            //Make sure it's set to exactly 1f
            fade = 1f;
            spriteColor.a = fade;
            sprite.color = spriteColor;
            yield return new WaitForSeconds(delayToFadeOut);

            startTime = Time.time;
            while (fade > 0f)
            {
                fade = Mathf.Lerp(1f, 0f, (Time.time - startTime) / fadeOutTime);
                spriteColor.a = fade;
                sprite.color = spriteColor;
                yield return null;
            }
            fade = 0f;
            spriteColor.a = fade;
            sprite.color = spriteColor;
            yield return new WaitForSeconds(delayToFadeIn);
        }
    }
}