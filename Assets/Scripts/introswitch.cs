﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class introswitch : MonoBehaviour {
    public GameObject intro;
    public GameObject tut1, tut2, gl;
    public Button Btn;
    int index=0;
    public void change() {
        index++;
        switch (index) {
            case 1: intro.SetActive(false);
                tut1.SetActive(true);
                break;
            case 2:
                tut1.SetActive(false);
                tut2.SetActive(true);
                break;
            case 3:
                tut2.SetActive(false);
                gl.SetActive(true);
                break;
            case 4:
                SceneManager.LoadScene("Play");
                break;
            default: break;
        }
    }
}
