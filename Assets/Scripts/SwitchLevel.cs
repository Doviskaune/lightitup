﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class SwitchLevel : MonoBehaviour
{
	public AnimationCurve slideCurve = new AnimationCurve(new Keyframe(0, 1, 0, -2.128378f), new Keyframe(1.5f, 0, 0, 0));
	public AnimationCurve fadeCurve = new AnimationCurve(new Keyframe(0, 1, 0, -3f), new Keyframe(0.5f, 0, -3f, 0));

	private float deltaY = 10;
	private Transform finish;
	private SpriteRenderer finishLamp;

	private GameObject[] gameLevels;
	public int currentLevelId = 0;
	public GameObject currentLevel = null;
	private SpriteRenderer[] sprites;

	private float animationStartTime;

	public Text score;

	public GameObject photon;

	private void Start()
	{
		gameLevels = Resources.LoadAll("Levels", typeof(GameObject)).Cast<GameObject>().ToArray();
		currentLevelId = PlayerPrefs.GetInt("level", 0);
		currentLevelId--;
		NextLevel();
	}

	public void LevelCompleted()
	{
		animationStartTime = Time.time;
		StartCoroutine(LightAnimationUpdate());
		finish.GetComponent<FadeInOut>().animate = false;
	}

	private IEnumerator LightAnimationUpdate()
	{
		while ((Time.time - animationStartTime) <= photon.GetComponent<TrailRenderer>().time)
		{
			Color c = finishLamp.color;
			c.a = (Time.time - animationStartTime) / photon.GetComponent<TrailRenderer>().time;
			Debug.Log(c.a);
			finishLamp.color = c;
			if(SoundEffects.instance!=null &&
				(Time.time - animationStartTime) >= photon.GetComponent<TrailRenderer>().time/2
				&& (Time.time - animationStartTime - Time.deltaTime) <= photon.GetComponent<TrailRenderer>().time/2)
				SoundEffects.instance.playLevelSuccess();
			yield return null;
		}

		yield return new WaitForSeconds(1);
		FadeOutAnimationStart();
	}

	private void FadeOutAnimationStart()
	{
		animationStartTime = Time.time;
		StartCoroutine(FadeAnimationUpdate());
	}

	private IEnumerator FadeAnimationUpdate()
	{
		while (Time.time - animationStartTime <= fadeCurve[fadeCurve.length - 1].time)
		{
			foreach (SpriteRenderer ren in sprites)
			{
				Color c = ren.material.color;
				c.a = fadeCurve.Evaluate(Time.time - animationStartTime);
				ren.material.color = c;
			}

			yield return null;
		}

		currentLevel.SetActive(false);
		Destroy(currentLevel);
		sprites = null;

		NextLevel();
		yield return null;
	}

	public void NextLevel()
	{
		PlayerPrefs.SetInt("level", ++currentLevelId);
		score.text = currentLevelId.ToString();

		if (currentLevelId >= gameLevels.Length)
		{
			currentLevelId = 0;
		}

		currentLevel = Instantiate(gameLevels[currentLevelId],
								   new Vector3(0, deltaY, 0), Quaternion.identity);

		animationStartTime = Time.time;
		StartCoroutine(SlideAnimationUpdate());
	}

	private IEnumerator SlideAnimationUpdate()
	{
		while (Time.time - animationStartTime <= slideCurve[slideCurve.length - 1].time)
		{
			currentLevel.transform.position = new Vector3(currentLevel.transform.position.x,
					slideCurve.Evaluate(Time.time - animationStartTime) * deltaY,
					currentLevel.transform.position.z);
			yield return null;
		}

		finish = currentLevel.transform.GetChild(0).GetChild(0);
		sprites = currentLevel.transform.GetComponentsInChildren<SpriteRenderer>();
		finish.GetComponent<FadeInOut>().StartAnimation();
		finishLamp = currentLevel.transform.GetChild(0).GetChild(2).GetComponent<SpriteRenderer>();

		GetComponent<PlayerController>().TurnOn();
		yield return null;
	}

	public bool LevelFailed()
	{
		if (currentLevelId == 0)
			return false;

		animationStartTime = Time.time;
		StartCoroutine(SlideUpAnimationUpdate());
		finish.GetComponent<FadeInOut>().animate = false;
		return true;
	}

	private IEnumerator SlideUpAnimationUpdate()
	{
		while (Time.time - animationStartTime <= slideCurve[slideCurve.length - 1].time)
		{
			currentLevel.transform.position = new Vector3(currentLevel.transform.position.x,
					deltaY - slideCurve.Evaluate(Time.time - animationStartTime) * deltaY,
					currentLevel.transform.position.z);
			yield return null;
		}

		currentLevel.SetActive(false);
		Destroy(currentLevel);
		sprites = null;

		PreviousLevel();
	}

	private void PreviousLevel()
	{
		PlayerPrefs.SetInt("level", --currentLevelId);
		score.text = currentLevelId.ToString();

		currentLevel = Instantiate(gameLevels[currentLevelId],
								   new Vector3(0, 0, 0), Quaternion.identity);

		animationStartTime = Time.time;

		finish = currentLevel.transform.GetChild(0).GetChild(0);
		sprites = currentLevel.transform.GetComponentsInChildren<SpriteRenderer>();
		finishLamp = currentLevel.transform.GetChild(0).GetChild(2).GetComponent<SpriteRenderer>();

		StartCoroutine(FadeInAnimationUpdate());
	}

	private IEnumerator FadeInAnimationUpdate()
	{
		while (Time.time - animationStartTime <= fadeCurve[fadeCurve.length - 1].time)
		{
			foreach (SpriteRenderer ren in sprites)
			{
				Color c = ren.material.color;
				c.a = 1 - fadeCurve.Evaluate(Time.time - animationStartTime);
				ren.material.color = c;
			}

			yield return null;
		}

		finish.GetComponent<FadeInOut>().StartAnimation();
		GetComponent<PlayerController>().TurnOn();
	}
}