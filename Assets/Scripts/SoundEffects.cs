﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundEffects : MonoBehaviour {
	public AudioSource buttonClick;
	public AudioSource success;

	public static SoundEffects instance;

	void Awake() {
		instance = this;
	}

	public void playOnButtonClick() {
		buttonClick.Play();
	}

	public void playLevelSuccess() {
		success.Play();
	}
}
