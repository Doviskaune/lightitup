﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenu : MonoBehaviour {
    public void StopTime()
    {
        Time.timeScale = 0.0f;
    }
    
    public void ResumeTime()
    {
        Time.timeScale = 1.0f;
    }
}
