﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class FollowPath : MonoBehaviour
{
    private Action disapearCallback;
    private Action finishCallback;
    public Vector3[] path = null;

    public int currentPoint;
    private Vector3 targetPoint;

    public float speed = 10f;

    public void StartAnimation(Vector3[] path, Action disapear, Action finish)
    {
        disapearCallback = disapear;
        finishCallback = finish;
        currentPoint = 0;
        this.path = path;
    }

    private void Update()
    {
        if (path == null)
            return;

        if (currentPoint < path.Length)
        {
            targetPoint = path[currentPoint];
            walk();
        }
        else if (path.Length > 0)
        {
            path = null;
            StartCoroutine(TrailExited());
            finishCallback();
        }
    }

    private void walk()
    {
        transform.forward = Vector3.RotateTowards(transform.forward,
                            targetPoint - transform.position, speed * Time.deltaTime, 0);

        transform.position = Vector3.MoveTowards(transform.position, targetPoint, speed * Time.deltaTime);

        if (transform.position == targetPoint)
        {
            currentPoint++;
        }
    }

    private IEnumerator TrailExited()
    {
        yield return new WaitForSeconds(GetComponent<TrailRenderer>().time + 0.01f);
        disapearCallback();
        gameObject.SetActive(false);
        Destroy(gameObject);
    }

    private void OnBecameInvisible()
    {
        if (path != null && gameObject.active)
        {
            StartCoroutine(TrailExited());
        }
    }
}