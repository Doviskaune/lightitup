using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class PlayerController : MonoBehaviour
{
	public Material lightbulbON;
	public Material lightbulbOFF;

	public Material buttonbulbON;
	public Material buttonbulbOFF;

	public bool working = true;
	private bool movable = true;

	private const int rotationOffset = 90;
	public Renderer lightbulbRenderer;
	public Renderer buttonRenderer;
	private bool mouseDown = false;

	public GameObject photon;
	public int reflectCount = 100;

	[Range(0, 2)]
	public float constant = 1.1f;

	private LineRenderer line;

	private bool hit = false;
	private bool animationStarted = false;

	private void Start()
	{
		line = GetComponent<LineRenderer>();
	}

	private void Update()
    {
        Debug.Log(Input.mousePosition.y);
        if (!movable || Pause_btn.isPause)
			return;

		Vector3 difference = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
		difference.Normalize();

		float rotZ = Mathf.Atan2(difference.y, difference.x) * Mathf.Rad2Deg;
		transform.rotation = Quaternion.Euler(0f, 0f, rotZ + rotationOffset);
		if (working && !mouseDown && Input.GetMouseButtonDown(0) && Input.mousePosition.y <= 1600)
		{
			Aim();
			mouseDown = true;

			ChangeMaterial(lightbulbRenderer, lightbulbON, 0);
		}
		else if (working && mouseDown && Input.GetMouseButtonUp(0))
		{
			Shoot();
			mouseDown = false;
			working = false;

			ChangeMaterial(lightbulbRenderer, lightbulbOFF, 0);
			ChangeMaterial(buttonRenderer, buttonbulbOFF, 0);
		}

		if (line.enabled) Aim();
	}

	private void ChangeMaterial(Renderer renderer, Material material, int index)
	{
		Material[] mats = renderer.materials;
		mats[index] = material;
		renderer.materials = mats;
	}

	public void Aim()
	{
		line.enabled = true;
		var beam = LightBeam.Beam(
					   photon.transform.position,
					   transform.up, //transform.position - mousePos,
					   reflectCount,
					   constant);

		line.positionCount = beam.Length;
		line.SetPositions(beam);
	}

	public void Shoot()
	{
		var beamShot = LightBeam.BeamShot(
						   photon.transform.position,
						   transform.up, //transform.position - mousePos,
						   reflectCount,
						   constant);
		hit = LightBeam.finished && beamShot.Length >= GetComponent<SwitchLevel>().currentLevel.transform.childCount;

		Instantiate(photon).GetComponent<FollowPath>().StartAnimation(beamShot, BeamDisapeard, LightOnAnimation);
		line.enabled = false;
		movable = false;
		animationStarted = false;
	}

	public void BeamDisapeard()
	{
		movable = true;

		if (animationStarted)
		{
			return;
		}

		animationStarted = true;

		if (hit)
		{
			GetComponent<SwitchLevel>().LevelCompleted();
		}
		else
		{
			if (!GetComponent<SwitchLevel>().LevelFailed()) {
				working = true;
			}
		}
	}

	public void TurnOn()
	{
		ChangeMaterial(buttonRenderer, buttonbulbON, 0);
		working = true;
	}

	public void LightOnAnimation()
	{
		if (animationStarted)
		{
			return;
		}

		if (hit)
		{
			GetComponent<SwitchLevel>().LevelCompleted();
			animationStarted = true;
		}
	}
}