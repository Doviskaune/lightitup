﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Pause_btn : MonoBehaviour
{
    public static bool isPause = false;
    public GameObject PauseMeniuUI;

    // Update is called once per frame
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (isPause)
            {
                Resume();
            }
            else
            {
                Pause();
            }
        }
    }

    public void Resume()
    {
        PauseMeniuUI.SetActive(false);
        Time.timeScale = 1f;
        isPause = false;
    }

    public void Pause()
    {
        PauseMeniuUI.SetActive(true);
        Time.timeScale = 0f;
        isPause = true;
    }

    public void loadMenu()
    {
        StopGameCoroutines();
        Time.timeScale = 1f;
        SceneManager.LoadScene("Main_menu");
    }

    public void StopGameCoroutines()
    {
        {
            FollowPath t = FindObjectOfType<FollowPath>();
            if (t != null)
                t.StopAllCoroutines();
        }

        {
            SwitchLevel t = FindObjectOfType<SwitchLevel>();
            if (t != null)
                t.StopAllCoroutines();
        }
    }

    public void loadOptions()
    {
    }

    public void quitGame()
    {
        StopGameCoroutines();
        Application.Quit();
    }

    private void OnDisable()
    {
        isPause = false;
    }
}