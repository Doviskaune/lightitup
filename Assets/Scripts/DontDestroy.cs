﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class DontDestroy : MonoBehaviour {
    static bool AudioBegin = false;
    private AudioSource audio;

    void Awake()
    {
        audio = GetComponent<AudioSource>();

        if (!AudioBegin)
        {
            audio.Play();
            DontDestroyOnLoad(gameObject);
            AudioBegin = true;
        }
    }
}
